from itertools import product

c = get_config()
c.robot_pos = (0.,0.)
c.robot_angle = 0
c.robot_type = DefaultRobotWithDraw
#c.robot_linear_damping = 1.0
c.robot_linear_damping = 0.9
#c.robot_angluar_damping = 1.0
c.robot_angluar_damping = 0.99
c.robot_restitution = 0.1
c.world_limits = None
c.timestep = 0.1
c.vel_iters = 10
c.pos_iters = 8
c.obstacles = [[(-2,0.5), (0,0.5), (0,0.6), (-2,0.6) ] ]
c.world_limit = [[-2, -0.5], [1, 2]]
c.init_pos = [(0.25, 0.5)]#, (-1.5,-0.15)]
c.init_angle = 0.
c.target_pos = [-1.5,1.5]
c.target_rad = 0.5
