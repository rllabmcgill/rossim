import sys
sys.path.append('gen-py')
from robot import Physics
from robot.ttypes import *
from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol

try:
  transport = TSocket.TSocket('localhost', 7000)
  transport = TTransport.TBufferedTransport(transport)
  protocol = TBinaryProtocol.TBinaryProtocol(transport)

  client = Physics.Client(protocol)

  transport.open()

  client.angle(0.5)
  for i in range(100):
      client.inputs([0.7, 0.7])
      print client.outputs()

  transport.close()

except Thrift.TException, tx:
  print '%s' % (tx.message)

