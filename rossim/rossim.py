#!/usr/bin/env python

""" Rossim: a 2D robot simulator

Usage:
  rossim.py [--config=<FILE> --output=<FILE>] <command> [<args>...]

Options:
  -h, --help       This help message.
  --config=<FILE>  Optional configuration file.
  --output=<FILE>  Record trajectory to file.

The most commonly used rossim commands are:
  thrift     Expose the environment over Apache Thrift
  teleop  Renders a keyboard-controlled environment

See 'rossim.py help <command>' for more information on a specific command.

"""
from visualize import DefaultRobotWithDraw
from docopt import docopt
from robot2d import Environment
from itertools import product

class RossimConfig:

    """ Exposes the user-definable parameters
    """

    def __init__(self):
        self.robot_pos = (0,0)
        self.robot_angle = 0
        self.robot_type = DefaultRobotWithDraw
        self.robot_linear_damping = 0.9
        self.robot_angluar_damping = 0.99
        self.robot_restitution = 0.1
        # Parameters for the Simulation, avoid changing these while the
        # simulation is running.
        # Simulated elapsed time per step, increasing this sacrifices accuracy
        # for more performance
        self.timestep = 1./10.
        # number of iterations of the solvers, lowering this sacrifices accuracy
        # for more performance
        self.vel_iters = 10
        self.pos_iters = 8
        # world limit should be a list with two tuple. The coordinates
        # of the bottom left limit and of the top right limit
        self.world_limit = [[-10, -10], [50, 50]]
        #self.obstacles = [[ (1,1), (2,5), (2,3)], [ (10,10), (20,50), (20,30)]]
        self.obstacles = []
        #self.target_pos = [-0.5, -0.5]
        #self.target_rad =  0.4
        self.init_pos = list(product((-1,1),(-1,1)))
        self.init_angle = 0.

class FileConfigLoader:

    """ Exposes a "get_config()" factory method
    from any configuration file. The purpose of this method is to fill
    provide default values for the unspecified attributes of the config object.
    """

    def __init__(self, filename=None):
        """
        :param filename: Path to the configuration file.
        :type filename: str.
        """
        self.config = RossimConfig()

        def get_config():
            """ A closured passed to the config file in its namespace"""
            return self.config

        if filename:
            execfile(filename, dict(get_config=get_config, DefaultRobotWithDraw=DefaultRobotWithDraw))

    def generate(self):
        """ Generates a config file from a config object """
        print 'c = get_config()'
        for key, value in self.config.__dict__.iteritems():
           print 'c.{0} = {1}'.format(key, value)

if __name__ == '__main__':
    args = docopt(__doc__, options_first=True)

    config_loader = FileConfigLoader(args['--config'])
    config = config_loader.config

    robot = config.robot_type()
    env = Environment(robot, **vars(config))

    argv = [args['<command>']] + args['<args>']

    if args['<command>'] == 'thrift':
        import rpc.thrift.server as thr
        args = docopt(thr.__doc__, argv=argv)
        if args['--visualize']:
            import visualize
            visualize.run_threaded_pyglet(env, robot)
        thr.socket_serve(env, robot, args['--port'])
    elif args['<command>'] == 'teleop':
        import visualize
        args = docopt(visualize.__doc__, argv=argv)
        visualize.run_pyglet(env, robot, True)

