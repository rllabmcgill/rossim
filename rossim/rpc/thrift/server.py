"""usage: rossim.py thrift [--port=<port> --host=<host> --visualize]

Options:
  --port=<port>  Port on which the server should listen [default: 7000]
  --host=<host>  Hostname to which the server should bind to [default: localhost]
  --visualize    Visualize the robot in a non-interactive window [default: False]

"""
import sys

from proxy.robot import Physics
from proxy.robot.ttypes import *
from docopt import docopt
from thrift import Thrift
from thrift.server import TServer
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol

class RobotHandler:
  def __init__(self, env, robot):
    self.environment = env
    self.robot = robot
    self.environment.step(timestep=0.1)

  def angle(self, rad):
    print 'Turn {0}'.format(rad)
    #self.robot.angle = rad
    self.environment.step(timestep=0.1)

  def inputs(self, forces):
    print forces
    self.robot.drive_motors(forces)
    self.environment.step(timestep=0.1)

  def outputs(self):
    return self.robot.read_sensors()

def socket_serve(environment, robot, port):
    handler = RobotHandler(environment, robot)
    processor = Physics.Processor(handler)
    transport = TSocket.TServerSocket(port=port)
    tfactory = TTransport.TBufferedTransportFactory()
    pfactory = TBinaryProtocol.TBinaryProtocolFactory()

    server = TServer.TSimpleServer(processor, transport, tfactory, pfactory)

    print "Starting python server..."
    server.serve()
