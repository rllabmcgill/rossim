import Box2D
import numpy
import pdb
import math
from Box2D import b2World, b2ChainShape, b2Dot, b2Vec2
from Box2D import b2ContactListener, b2RayCastCallback

class LaserRangeFinder(b2RayCastCallback):

    """ Simulate a laser ranger finder through raycasting
    """

    def __init__(self):
        b2RayCastCallback.__init__(self)
        self.point = None

    def ReportFixture(self, fixture, point, normal, fraction):
        self.point = point
        return fraction

    def getRange(self, world, point_from, point_to):
        self.point = None
        world.RayCast(self, point_from, point_to)

        if self.point == None:
            return -1

        return math.sqrt( (self.point[0] - point_from[0])**2 +
                          (self.point[1] - point_from[1])**2)


class Robot(b2ContactListener):

    """ Abstract robot class"""

    def drive_motors(self, forces):
        """ Apply forces to the motors

        :param forces: A list of forces for each motor.
        :type forces: list.

        .. note:: Needs to be overridden

        """
        pass

    def read_sensors(self):
        """ Query all of the available sensors
        :returns: list of sensor readings

        .. note:: Needs to be overridden

        """
        pass

    def attach_body(self, world, **kwargs):
        """ Ground the robot body into the world

        .. note:: Needs to be overridden

        """
        pass

    @property
    def body(self):
        """ The robot body
        :type: Box2D

        .. note:: Needs to be overridden

        """
        pass

    @property
    def velocity(self):
        """ The velocity of the body in the world
        """
        return self.body.linearVelocity

    @velocity.setter
    def velocity(self, vel):
        """ Set the new linear velocity.
        """
        self.body.linearVelocity = vel

    @property
    def angular_velocity(self):
        """ The angular velocity of the body in the world
        """
        return self.body.angularVelocity

    @angular_velocity.setter
    def angular_velocity(self, vel):
        """ Set the angular velocity.
        """
        self.body.angularVelocity = vel

    @property
    def position(self):
        """ The position of the body in the world frame
        """
        return self.body.position

    @position.setter
    def position(self, pos):
        """ Set the new position. This is an instanteneous
        property and won't result in a smooth and realistic
        translation.
        """
        self.body.position = pos

    @property
    def angle(self):
        """ The angle of the body in the world frame
        """
        return self.body.angle

    @angle.setter
    def angle(self, rad):
        """ Set the new angle. This is an instanteneous
        property and won't result in a smooth and realistic
        rotation.
        """
        self.body.angle = rad

class RoombaRobot(Robot):

    """ Basic circular robot with differential drive

    It also has bumpers sensors and lasers.

    """

    min_force=-1
    max_force= 1
    vertex_count = 40

    bumper_h = 0.05
    bumper_w = 0.1

    laser_max_range = 10

    def __init__(self, radius=0.17, nbumpers = 6, nlasers= 16, density=8.0):
        b2ContactListener.__init__(self)

        self.radius = radius
        self.inputs = self.default_input()
        self.applypoints = ((-radius, 0), (radius, 0))
        self.vertices = numpy.array([(0,0)]+[(math.sin(x)*radius, math.cos(x)*radius)
                        for x in numpy.linspace(0, math.pi*2, self.vertex_count)])
        self.nlasers = nlasers
        self.nbumpers = nbumpers
        self.laserrange = LaserRangeFinder()

        self.output = numpy.zeros(nlasers + nbumpers)

        self.density = density

    @property
    def body(self):
        return self.robotBody

    def drive_motors(self, forces):
        self.inputs = numpy.array(forces)

    def read_sensors(self):
        self.update_output(self.world)
        return self.output

    def attach_body(self, world, **kwargs):
        self.world = world
        robotBody = world.CreateDynamicBody(**kwargs)
        robotBody.CreateFixture( shape = Box2D.b2CircleShape(pos=(0, 0),
                    radius = self.radius), density=self.density, **kwargs)

        for i, angle in enumerate(numpy.linspace(0, math.pi*2, self.nbumpers+1)[:-1]):
            c = numpy.array((self.radius * math.sin(angle),
                              self.radius * math.cos(angle)))
            robotBody.CreateFixture(shape = Box2D.b2PolygonShape(
                                                        box = (self.bumper_w/2,
                                                               self.bumper_h/2,
                                                        (c[0], c[1]),
                                                        angle)),
                                    density = 0.01,
                                    isSensor = True,
                                    userData = 'b'+ str(i))
        self.robotBody = robotBody
        return robotBody

    def apply_forces(self):
        robotBody = self.robotBody
        numpy.clip(self.inputs, self.min_force, self.max_force, self.inputs)
        normal = robotBody.GetWorldVector((1,0))
        lateralimp = -robotBody.mass* b2Dot(normal, robotBody.linearVelocity) * normal
        robotBody.ApplyLinearImpulse(impulse= lateralimp,
                                      point= robotBody.worldCenter,
                                      wake = True)
        for i in range(2):
            robotBody.ApplyForce(force = robotBody.GetWorldVector((0, self.inputs[i])),
                                 point = robotBody.GetWorldPoint(self.applypoints[i]),
                                 wake = True )

    def brake(self):
        robotBody = self.robotBody
        numpy.clip(self.inputs, self.min_force, self.max_force, self.inputs)
        normal = robotBody.GetWorldVector((0,1))
        #lateralimp = -robotBody.mass* b2Dot(normal, robotBody.linearVelocity) * normal
        lateralimp = -robotBody.mass*robotBody.linearVelocity
        robotBody.ApplyLinearImpulse(impulse= lateralimp,
                                      point= robotBody.worldCenter,
                                      wake = True)
    def default_input(self):
        return numpy.zeros(2);

    def update_output(self, world):
        for i, angle in enumerate(numpy.linspace(0, math.pi*2, self.nlasers+1)[:-1]):
            pfrom, pto = self._make_ray(angle)
            self.output[i+self.nbumpers] = self.laserrange.getRange(world,
                                                 self.robotBody.GetWorldPoint(pfrom),
                                                 self.robotBody.GetWorldPoint(pto))
            if self.output[i + self.nbumpers]< 0:
                self.output[i + self.nbumpers] = self.laser_max_range

    def _make_ray(self, angle):
        """ Used to set up the raytracer
        :param angle: the angle in which the laser is oriented
        :returns: the starting and ending points of the ray. Tuple of tuples
        """
        pfrom = numpy.array((self.radius * math.sin(angle),
                             self.radius * math.cos(angle)))
        pto = pfrom*(self.laser_max_range/numpy.linalg.norm(pfrom) + 1)
        return pfrom, pto

    def BeginContact(self, contact):
        uA = contact.fixtureA.userData
        uB = contact.fixtureB.userData
        index = -1
        if isinstance(uA, basestring) and uA[0] == 'b':
            index = int(uA[1:])
        elif isinstance(uB, basestring) and uB[0] == 'b':
            index = int(uB[1:])

        if index > -1:
            self.output[index] = 1

    def EndContact(self, contact):
        uA = contact.fixtureA.userData
        uB = contact.fixtureB.userData
        index = -1
        if isinstance(uA, basestring) and uA[0] == 'b':
            index = int(uA[1:])
        elif isinstance(uB, basestring) and uB[0] == 'b':
            index = int(uB[1:])

        if index > -1:
            self.output[index] = 0

class Environment:

    """ This class provides the physics simulation of the
    robot in its environment. An environment consists of a
    robot and a set of obstacles with which it interacts.
    """

    def __init__(self, robot, **kwargs):
        self.robot = robot

        robot_pos = kwargs.get('robot_pos', (0,0))
        robot_angle = kwargs.get('robot_angle', 0)
        robot_linear_damping = kwargs.get('robot_linear_damping', 0.9)
        robot_angluar_damping = kwargs.get('robot_angluar_damping', 0.99)
        robot_restitution = kwargs.get('robot_restitution', 0.1)

        world_limits = kwargs.get('world_limit', None)
        self.timestep = kwargs.get('timestep', 1/10)
        self.vel_iters = kwargs.get('vel_iters', 10)
        self.pos_iters = kwargs.get('pos_iters', 8)

        # initialize an empty world
        if isinstance(self.robot, b2ContactListener):
            self.world = b2World(contactListener = self.robot,
                                  gravity = (0,0),
                                  doSleep = True)
        else:
            self.world = b2World(gravity = (0,0), doSleep = True)

        # create obstacle shapes from list of vertex list
        self.__obstacle_vertices = kwargs.get('obstacles', [])
        shapes = [ b2ChainShape(vertices_loop = v) for v in self.__obstacle_vertices]

        # create target vertices if specified
        self.__target_vertices = []
        self.__target_indices = []
        if 'target_pos' in kwargs and 'target_rad' in kwargs:
            self.__target_vertices = numpy.asarray(kwargs['target_pos'])+\
                              numpy.array([(math.sin(x)*kwargs['target_rad'], math.cos(x)*kwargs['target_rad'])
                                  for x in numpy.linspace(0, math.pi*2, 40)])
            self.__target_indices = [[0]] + [[x, x] for x in range(1, len(self.__target_vertices)) ] + [[0]]
            self.__target_indices = [x for l in self.__target_indices for x in l]
            self.__target_vertices = [ x for p in self.__target_vertices for x in p]

        # if limits were specified, create obstacle boundary
        if world_limits != None:
            v= [world_limits[0], (world_limits[1][0], world_limits[0][1]),
                   world_limits[1],  (world_limits[0][0], world_limits[1][1])]
            self.__obstacle_vertices.append(v)

            shapes.append(b2ChainShape(vertices_loop = v))

        # create an unmovable entity. This will represent all obstacles
        self.obstaclebody = self.world.CreateStaticBody(shapes = shapes)

        # package the robot physical properties (for collisions)
        # The robot constructor can choose to ignore them
        # The following change of variable names is needed beacause
        # of the naming convention in Box2d.
        param = {'position': robot_pos,
                    'angle': robot_angle,
                    'linearDamping': robot_linear_damping,
                    'angularDamping': robot_angluar_damping,
                    'restitution': robot_restitution}

        # create the robot entity
        self.robotbody = self.robot.attach_body(self.world, **param)

    def step(self, timestep=None):
        # apply various forces (e.g. robot actuator)
        self.robot.apply_forces()

        # simulate one step of the world
        if timestep == None:
            timestep = self.timestep
        self.world.Step(timestep, self.vel_iters, self.pos_iters)

        # clear forces
        self.world.ClearForces()

    @property
    def obstacle_vertices(self):
        return self.__obstacle_vertices

    @property
    def target_vertices(self):
        return self.__target_vertices

    @property
    def target_indices(self):
        return self.__target_indices
