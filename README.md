rossim
======

A 2D robot simulator with physics simulation supported by Box2D.

rossim aims to provide a lean faster-than-realtime simulator without
any coupling to the visualization component or specific IPC/RPC.

![](./doc/screenshot.png)
